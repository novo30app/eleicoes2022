<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/login/', fn(Request $request, Response $response) => $this->LoginController->login($request, $response));

$app->post('/login/', fn(Request $request, Response $response) => $this->LoginController->autentication($request, $response));

$app->get('/logout/', fn(Request $request, Response $response) => $this->LoginController->logout($request, $response));

$app->get('/ativar/{id}/', fn(Request $request, Response $response) => $this->LoginController->changeActive($request, $response));

