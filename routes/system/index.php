<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/', fn (Request $request, Response $response) => $this->UserController->index($request, $response));

$app->get('/perfil/', fn (Request $request, Response $response) => $this->UserController->profile($request, $response));

$app->get('/edita-perfil/{id}/', fn (Request $request, Response $response) => $this->UserController->profile($request, $response));

$app->get('/candidatos/', fn (Request $request, Response $response) => $this->UserController->candidates($request, $response));

$app->get('/visualiza/', fn (Request $request, Response $response) => $this->UserController->view($request, $response));

$app->get('/visualizar-perfil/{id}/', fn (Request $request, Response $response) => $this->UserController->view($request, $response));

$app->get('/requerimentos/', fn (Request $request, Response $response) => $this->UserController->requirements($request, $response));

$app->get('/eleitos/', fn (Request $request, Response $response) => $this->UserController->elected($request, $response));

$app->get('/visualiza-eleito/{id}/', fn (Request $request, Response $response) => $this->UserController->viewElected($request, $response));

$app->get('/visualiza-eleito/', fn (Request $request, Response $response) => $this->UserController->viewElected($request, $response));