<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/usuarios/', fn (Request $request, Response $response) => $this->UsersAdminController->index($request, $response));

$app->get('/candidato/{id}', fn (Request $request, Response $response) => $this->UserController->candidates($request, $response));

