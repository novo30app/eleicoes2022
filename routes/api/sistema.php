<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->group('/api/sistema', function () use ($app) {

    $app->get('/cidades/{state}/', function (Request $request, Response $response) {
        return $this->ApiController->getCitiesByState($request, $response);
    });

    $app->options('/cidades/{state}/', function (Request $request, Response $response) {
        return;
    });

});