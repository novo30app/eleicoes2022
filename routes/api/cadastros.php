<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/cadastros', function () use ($app) {

    $app->get('/usuarios/[{id}/]', fn(Request $request, Response $response) => $this->ApiController->users($request, $response));

    $app->post('/usuarios/', fn(Request $request, Response $response) => $this->UsersAdminController->save($request, $response));

    $app->post('/altera-status/{id}/', fn(Request $request, Response $response) => $this->UsersAdminController->changeActive($request, $response));

    $app->get('/candidatos/[{id}/]', fn(Request $request, Response $response) => $this->ApiController->candidates($request, $response));
    
    $app->get('/requerimentos/', fn(Request $request, Response $response) => $this->ApiController->requirements($request, $response));

    $app->get('/eleitos/', fn(Request $request, Response $response) => $this->ApiController->elected($request, $response));

    $app->post('/instrucoes/', fn(Request $request, Response $response) => $this->UserController->instructions($request, $response));

    $app->post('/eleitos-atualiza/', fn(Request $request, Response $response) => $this->UserController->refreshElected($request, $response));

});