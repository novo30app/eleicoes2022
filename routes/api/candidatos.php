<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/candidatos', function () use ($app) {

    $app->post('/informacoes-pessoais/', fn(Request $request, Response $response) => $this->UserController->personalInformation($request, $response));

    $app->post('/informacoes-campanha/', fn(Request $request, Response $response) => $this->UserController->campaigInformation($request, $response));

    $app->post('/redes-sociais/', fn(Request $request, Response $response) => $this->UserController->socialMedia($request, $response));

    $app->post('/dados-bancarios/', fn(Request $request, Response $response) => $this->UserController->bankData($request, $response));

    $app->post('/bandeiras/', fn(Request $request, Response $response) => $this->UserController->flags($request, $response));

    $app->post('/foto-oficial/', fn(Request $request, Response $response) => $this->UserController->photograph($request, $response));

    $app->put('/valida-foto/', fn(Request $request, Response $response) => $this->UserController->photographValid($request, $response));

    $app->post('/video/', fn(Request $request, Response $response) => $this->UserController->video($request, $response));

    $app->put('/valida-video/', fn(Request $request, Response $response) => $this->UserController->videoValid($request, $response));

    $app->post('/qrcode/', fn(Request $request, Response $response) => $this->UserController->qrCode($request, $response));

    $app->post('/data-requerimento/', fn(Request $request, Response $response) => $this->UserController->requirementsDate($request, $response));

    $app->post('/visibilidade/{candidato}/', fn(Request $request, Response $response) => $this->UserController->visible($request, $response));

    $app->post('/status/{candidato}/', fn(Request $request, Response $response) => $this->UserController->setStatus($request, $response));

});