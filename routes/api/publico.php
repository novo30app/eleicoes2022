<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/publico', function () use ($app) {

    $app->get('/listagem/', fn(Request $request, Response $response) => $this->PublicController->getCandidateList($request, $response));

    $app->get('/listagem-filtro/', fn(Request $request, Response $response) => $this->PublicController->getCandidateListDetails($request, $response));

    $app->get('/listagem-designacao/', fn(Request $request, Response $response) => $this->PublicController->getCandidateListDesignation($request, $response));

    $app->get('/candidato/{slug}/', fn(Request $request, Response $response) => $this->PublicController->getCandidate($request, $response));

    $app->get('/bandeiras/', fn(Request $request, Response $response) => $this->PublicController->getFlags($request, $response));

    $app->get('/bandeiras-candidato/{candidato}/', fn(Request $request, Response $response) => $this->PublicController->getFlagsByCandidate($request, $response));
    
    $app->get('/pagina-requerimento/', fn(Request $request, Response $response) => $this->PublicController->getCandidateListRequirement($request, $response));

    $app->get('/mesorregiao/', fn(Request $request, Response $response) => $this->PublicController->getMeso($request, $response));

    $app->get('/cidades/', fn(Request $request, Response $response) => $this->PublicController->getCities($request, $response));

    $app->get('/cargos/', fn(Request $request, Response $response) => $this->PublicController->getPosts($request, $response));

    $app->get('/filtro-bandeiras/', fn(Request $request, Response $response) => $this->PublicController->getFlagsFilter($request, $response));

    $app->get('/suplentes/', fn(Request $request, Response $response) => $this->PublicController->getSuplent($request, $response));

    $app->get('/santinhos/', fn(Request $request, Response $response) => $this->PublicController->saint($request, $response));

    $app->get('/candidatos-santinhos/', fn(Request $request, Response $response) => $this->PublicController->saintCandidates($request, $response));

    $app->get('/listagem-eleitos/', fn(Request $request, Response $response) => $this->PublicController->getElectedList($request, $response));

    $app->get('/eleito/{slug}/', fn(Request $request, Response $response) => $this->PublicController->getElected($request, $response));

    $app->get('/cargo-eleito/{uf}/', fn(Request $request, Response $response) => $this->PublicController->getUfElected($request, $response));

    $app->post('/registra-requerimento/', fn(Request $request, Response $response) => $this->PublicController->saveRequirement($request, $response));

    $app->post('/altera-email/', fn(Request $request, Response $response) => $this->PublicController->changeEmail($request, $response));

});