<?php

namespace App\Controllers;
use App\Models\Entities\Users;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\Candidates;
use App\Models\Entities\RecoverPassword;
use App\Helpers\Utils;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Services\Auth;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class UsersAdminController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'admin/users.phtml', 'session' => 'users', 'title' => 'Listagem de Usuários', 'user' => $user, 'states' => $states]);
    }

    public function save(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'nameModal' => 'Nome',
                'emailModal' => 'E-mail',
                'typeModal' => 'Tipo',
                'stateModal' => 'Estado',
            ];
            Validator::requireValidator($fields, $data);
            $oldEmail = '';
            if (in_array($data['typeModal'], array(1, 4))) {
                $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['email' => $data['emailModal']]);
                if (!$candidate) throw new \Exception("Atenção, o cadastro deste candidato não foi encontrado no sistema, verifique se o e-mail está correto ou realize primeiro o registro dele na base!");
            }
            if ($data['userId'] > 0) {
                $userAdmin = $this->em->getRepository(Users::class)->findOneBy(['id' => $data['userId']]);
                $oldEmail = $userAdmin->getEmail();
            } else {
                $userCheck = $this->em->getRepository(Users::class)->findOneBy(['email' => $data['emailModal']]);
                if ($userCheck) throw new \Exception("Usuário já cadastrado!");
                $userAdmin = new Users;
            }
            $userAdmin->setType($data['typeModal'])
                        ->setName($data['nameModal'])
                        ->setEmail($data['emailModal'])
                        ->setState($this->em->getReference(State::class, $data['stateModal']))
                        ->setCreated(new \Datetime);
                        if($data['cityModal']) $userAdmin->setCity($this->em->getReference(City::class, $data['cityModal']));
            $userAdmin = $this->em->getRepository(Users::class)->save($userAdmin);
            if($data['userId'] == 0){
                $recoverPassword = new RecoverPassword();
                $recoverPassword->setUser($userAdmin)
                                ->setToken(Utils::generateToken())
                                ->setUsed(false)
                                ->setUserAdmin($user);
                $this->em->getRepository(RecoverPassword::class)->save($recoverPassword);
                $msg = Messages::userAdminMsg($userAdmin, $recoverPassword);
                Email::send($userAdmin->getEmail(), $userAdmin->getName(), 'Nova Conta - Admin Candidatos', $msg);    
            }
            Auth::sincronize($data['nameModal'], $data['emailModal'], $oldEmail);
            $this->em->commit();
            return $response->withJson([
            'status' => 'ok',
                'message' => "Usuário cadastrado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $userAdmin = $this->em->getRepository(Users::class)->find($id);
            $status = $userAdmin->isActive() == 0 ? 1 : 0;
            $userAdmin->setActive($status);
            $this->em->getRepository(Users::class)->save($userAdmin);
            $newStatus = $status == 1 ? 'activated' : 'disabled';
            $this->newAccessLog($this->getLogged(), 'changeAcess - ' . $id . ' - ' . $newStatus);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Usuário atualizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}