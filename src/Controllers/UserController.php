<?php

namespace App\Controllers;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\Banks;
use App\Models\Entities\Flags;
use App\Models\Entities\Users;
use App\Models\Entities\Elected;
use App\Models\Entities\FlagsList;
use App\Models\Entities\Candidates;
use App\Models\Entities\Instructions;
use App\Helpers\Session;
use App\Helpers\Validator;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class UserController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $instructions = $this->em->getRepository(Instructions::class)->find(1);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/index.phtml', 'session' => 'user', 'title' => 'Página Inicial', 
            'user' => $user, 'instructions' => $instructions]);
    }

    public function profile(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $candidate = $flags = null;
        if(in_array($user->getType(), array(1,4))){
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['email' => $user->getEmail()]);
            if (!$candidate) {
                Session::set('errorMsg', 'Perfil não encontrado!');
                $this->redirect();
            }            
            $flags = $this->em->getRepository(Flags::class)->findBy(['candidate' => $candidate->getId()]);
            $session = "profile";
        } else {
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $id]);
            $flags = $this->em->getRepository(Flags::class)->findBy(['candidate' => $id]);
            $session = "candidates";
        }
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(City::class)->findBy(['state' => $states]);
        $banks = $this->em->getRepository(Banks::class)->findAll();
        $flagsList = $this->em->getRepository(FlagsList::class)->list();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/profile.phtml', 'session' => $session, 'title' => 'Informações do Perfil', 'flagsList' => $flagsList, 
            'user' => $user, 'states' => $states, 'post' => $post, 'banks' => $banks, 'candidate' => $candidate, 'flags' => $flags, 'cities' => $cities]);
    }

    public function candidates(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        $flagsList = $this->em->getRepository(FlagsList::class)->list();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/candidates.phtml', 'session' => 'candidates', 'title' => 'Listagem de Candidatos', 
            'user' => $user, 'states' => $states, 'flagsList' => $flagsList]);
    }

    public function view(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $candidate = $flags = null;
        if(in_array($user->getType(), array(1, 4))){
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['email' => $user->getEmail()]);
            if (!$candidate) {
                Session::set('errorMsg', 'Perfil não encontrado!');
                $this->redirect();
            }
            $flags = $this->em->getRepository(Flags::class)->findBy(['candidate' => $candidate->getId()]);
            $session = "profile";
        } else {
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $id]);
            $flags = $this->em->getRepository(Flags::class)->findBy(['candidate' => $id]);
            $session = "candidates";
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/view.phtml', 'session' => $session, 'title' => 'Perfil', 'user' => $user, 
            'states' => $states, 'candidate' => $candidate, 'flags' => $flags]);
    }

    public function personalInformation(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'candidateId' => 'Candidato',
                'name' => 'Nome',
                'emailAccess' => 'emailAccess',
                'email' => 'E-mail',
                'post' => 'Cargo',
                'state' => 'Estado',
                'gender' => 'Genero',
            ];
            Validator::requireValidator($fields, $data);
            switch ($data['post']) {
                case 1:
                    $designation = 1;
                    break;
                case 2:
                    $designation = 2;
                    break;
                case 6:
                case 8:
                    $designation = 3;
                    break;
                case 3:
                case 9:
                    $designation = 4;
                    break;
                case 5:
                case 10:
                    $designation = 5;
                    break;
                default:
                    $designation = null;
                    break;
            }
            $city = $this->em->getRepository(City::class)->findOneBy(['id' => $data['city']]);
            $state = $this->em->getRepository(State::class)->findOneBy(['id' => $data['state']]);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setName($data['name'])
                        ->setEmail($data['emailAccess'])
                        ->setEmailCandidate($data['email'])
                        ->setPhone(preg_replace("/[^0-9]/", "",$data['phone']))
                        ->setPost($data['post'])
                        ->setDesignation($designation)
                        ->setState($state)
                        ->setProfession($data['profession'])
                        ->setSkinColor($data['skinColor'] ?: 0)
                        ->setGender($data['gender']);
                        if($data['city']) $candidate->setCity($city);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, $data);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function campaigInformation(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'candidateId' => 'Candidato',
                'nameMkt' => 'Nome Marketing',
                'resume' => 'Resumo',
            ];
            Validator::requireValidator($fields, $data);
            //$legend = $this->em->getRepository(Candidates::class)->findOneBy(['legend' => $data['numberLegend'], 'post' => [1,2]]);
            //if($legend) throw new \Exception("Este número de legenda já está sendo usado, verifique se está inserindo a informação correta!");
            $slug = str_replace(' ', '-', strtolower($this->_retiraAcentos($data['nameMkt'])));
            $site = $data['site'] ?: '';
            $proposal = $data['proposal'] ?: '';
            $crowdfunding = $data['crowdfunding'] ?: '';
            $requirementDate = $data['requirementDate'] ? new \Datetime(str_replace('/', '-', $data['requirementDate'])) : null;
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setNameMkt($data['nameMkt'])
                        ->setCrowdfunding($crowdfunding)
                        ->setSite($site)
                        ->setResume($data['resume'])
                        ->setProposals($proposal)
                        ->setRequirementDate($requirementDate);
                        if($data['numberLegend']) $candidate->setLegend($data['numberLegend']);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, $data);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function socialMedia(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setInstagram($data['instagram'])
                        ->setFacebook($data['facebook'])
                        ->setLinkedin($data['linkedin'])
                        ->setTwitter($data['twitter'])
                        ->setTikTok($data['tiktok'])
                        ->setYoutube($data['youtube']);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, $data);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function bankData(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'candidateId' => 'Candidato',
                'cnpj' => 'CNPJ de Campanha',
                'bank' => 'Banco',
                'agency' => 'Agencia',
                'account' => 'Conta',
            ];
            Validator::requireValidator($fields, $data);
            $bank = $this->em->getRepository(Banks::class)->findOneBy(['id' => $data['bank']]);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setCnpj($data['cnpj'])
                        ->setBank($bank)
                        ->setAgency($data['agency'])
                        ->setAccount($data['account']);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, $data);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function flags(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['candidateId' => 'Candidato','flagValue' => 'Bandeira'];
            Validator::requireValidator($fields, $data);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $this->em->getRepository(Flags::class)->refreshFlags($candidate);
            foreach($data['flagValue'] as $f){
                $checkFlag = $this->em->getRepository(Flags::class)->findOneBy(['candidate' => $data['candidateId'], 'flag' => $f]);
                if(!$checkFlag){
                    $id = $this->em->getRepository(FlagsList::class)->findOneBy(['id' => $f]);
                    $flag = new Flags();
                    $flag->setCandidate($candidate)
                            ->setFlag($id);
                    $this->em->getRepository(Flags::class)->save($flag);
                }
            }
            $this->saveUpdates($user, $candidate, $data['flagValue']);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function photograph(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['candidateId' => 'Candidato'];
            Validator::requireValidator($fields, $data);
            $file = $request->getUploadedFiles()['photograph'];
            if(!$file->getClientFilename()) throw new \Exception('Atenção, é obrigatório o anexo de uma foto válida!');
            $extension = explode('.', $file->getClientFilename());
            if (!in_array(end($extension), array('jpg', 'JPG', 'png', 'PNG', 'jpeg'))) throw new \Exception('Somente são aceitos arquivos em .png, .jpg, .jpeg');
            if($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
            $target = time() . '.' . end($extension);
            $file->moveTo("uploads/img/" . $target);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setPhotograph($target)->setPhotographValid(0);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('Inserção de Foto - ' . $target));
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function photographValid(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['candidateId' => 'Candidato'];
            Validator::requireValidator($fields, $data);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $status = $candidate->getPhotographValid() == 1 ? 0 : 1;
            $candidate->setPhotographValid($status);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('Validação de foto'));
            $message = $status == 1 ? 'validada' : 'invalidada';
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Foto ' . $message . ' com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function video(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['candidateId' => 'Candidato', 'video' => 'Link Video'];
            Validator::requireValidator($fields, $data);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setVideo($data['video'])->setVideoValid(0);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('Inserção de novo vídeo ' . $data['video']));
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function videoValid(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['candidateId' => 'Candidato'];
            Validator::requireValidator($fields, $data);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $status = $candidate->getVideoValid() == 1 ? 0 : 1;
            $candidate->setVideoValid($status);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('Validação de vídeo'));
            $message = $status == 1 ? 'validado' : 'invalidado';
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Video ' . $message . ' com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function qrCode(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['candidateId' => 'Candidato'];
            Validator::requireValidator($fields, $data);
            $file = $request->getUploadedFiles()['qrCode'];
            if(!$file->getClientFilename()) throw new \Exception('Atenção, é obrigatório o anexo de uma foto válida!');
            $extension = explode('.', $file->getClientFilename());
            if (!in_array(end($extension), array('jpg', 'JPG', 'png', 'PNG', 'jpeg'))) throw new \Exception('Somente são aceitos arquivos em .png, .jpg, .jpeg');
            if($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
            $target = time() . '.' . end($extension);
            $file->moveTo("uploads/img/" . $target);
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidateId']]);
            $candidate->setQrCode($target);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('Inserção de qrcode ' . $target));
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function requirements(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $candidates = $this->em->getRepository(Candidates::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/requirements.phtml', 'session' => 'requirements', 'title' => 'Requerimentos de Impugnação', 
            'user' => $user, 'candidates' => $candidates]);
    }

    public function instructions(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['instruction' => 'Instrução'];
            Validator::requireValidator($fields, $data);
            $user = $this->em->getRepository(Users::class)->findOneBy(['id' => $data['userId']]);
            $instruction = $this->em->getRepository(Instructions::class)->findOneBy(['id' => 1]);
            $instruction->setInstruction($data['instruction'])->setUser($user)->setModiFied(new \Datetime);
            $this->em->getRepository(Instructions::class)->save($instruction);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function requirementsDate(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['date' => 'Data Limite Impugnação', 'candidatesValue' => 'Candidato'];
            Validator::requireValidator($fields, $data);
            $date = new \Datetime($data['date']);
            foreach($data['candidatesValue'] as $c) {
                $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $c]);
                $candidate->setRequirementDate($date);
                $this->em->getRepository(Candidates::class)->save($candidate);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function visible(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('candidato');
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $id]);
            $status = $candidate->getVisible() == 1 ? 0 : 1;
            $candidate->setVisible($status)->setTerm($status);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('alteração de visibilidade ' . $status));
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Perfil Alterado com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function setStatus(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('candidato');
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $id]);
            $status = $candidate->getStatus() == 1 ? 0 : 1;
            $candidate->setStatus($status);
            $this->em->getRepository(Candidates::class)->save($candidate);
            $this->saveUpdates($user, $candidate, array('alteração de status ' . $status));
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cadastro Alterado com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function elected(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/elected.phtml', 'session' => 'elected', 'title' => 'Eleitos', 
            'user' => $user, 'states' => $states]);
    }

    public function viewElected(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $elected = null;
        if($id){
            $cities = $this->em->getRepository(City::class)->findBy(['state' => $states]);
            $elected = $this->em->getRepository(Elected::class)->findOneBy(['id' => $id]);    
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'user/viewElected.phtml', 'user' => $user, 
            'elected' => $elected, 'states' => $states, 'cities' => $cities]);
    }

    public function refreshElected(Request $request, Response $response)
    {
        try{
            $this->em->beginTransaction();
            $date = new \Datetime();
            $data = (array)$request->getParams();
            $file = $request->getUploadedFiles()['photograph'];
            $fields = ['name' => 'Nome', 'year' => 'Ano',  'post' => 'Cargo', 'state' => 'Estado', 'resume' => 'Resumo'];
            Validator::requireValidator($fields, $data);
            $votes = null;
            $active = $data['active'] ? 1 : 0;
            $reelected = $data['reelected'] ? 1 : 0;
            $slug = strtolower(str_replace(' ', '-', $data['name']));
            $city = $data['city'] ? $this->em->getRepository(City::class)->findOneBy(['id' => $data['city']]) : null;
            $state = $this->em->getRepository(State::class)->findOneBy(['id' => $data['state']]);
            $elected = $this->em->getRepository(Elected::class)->findOneBy(['id' => $data['id']]);
            $target = '';
            if($file->getClientFilename()){
                $extension = explode('.', $file->getClientFilename());
                if (!in_array(end($extension), array('jpg', 'JPG', 'png', 'PNG', 'jpeg'))) throw new \Exception('Somente são aceitos arquivos em .png, .jpg, .jpeg');
                if($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
                $target = time() . '.' . end($extension);
                $file->moveTo("uploads/img/" . $target);    
            }
            if(!$elected) {
                $elected = new Elected();
                $elected->setCreated($date);
            } 
            $elected->setActive($active)
                    ->setName($data['name'])
                    ->setYear($data['year'])
                    ->setReelected($reelected)
                    ->setPost($data['post'])
                    ->setVotes($data['votes'] ?? 0)
                    ->setState($state)
                    ->setResume($data['resume'])
                    ->setInstagram($data['instagram'])
                    ->setFacebook($data['facebook'])
                    ->setLinkedin($data['linkedin'])
                    ->setTwitter($data['twitter'])
                    ->setTiktok($data['tiktok'])
                    ->setYoutube($data['youtube'])
                    ->setEmail($data['email'])
                    ->setSite($data['site'])
                    ->setPhone($data['phone'])
                    ->setSlug($slug);
                    if($target != '') $elected->setPhotograph($target);
                    if($city) $elected->setCity($city);
            $this->em->getRepository(Elected::class)->save($elected);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}