<?php

namespace App\Controllers;
use App\Helpers\Utils;
use App\Models\Entities\Users;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\Elected;
use App\Models\Entities\Candidates;
use App\Models\Entities\Requirements;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiController extends Controller
{
    public function getCitiesByState(Request $request, Response $response)
    {
        $param = $request->getAttribute('route')->getArgument('state');
        $state = $this->em->getRepository(State::class)->find($param);
        if (!$state) $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $param]);
        $cities = $this->em->getRepository(City::class)->findBy(['state' => $state], ['cidade' => 'ASC']);
        $citiesArray = [];
        foreach ($cities as $city) {
            $citiesArray[] = ['id' => $city->getId(), 'name' => $city->getCidade()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $citiesArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function users(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $type = $request->getQueryParam('type');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $users = $this->em->getRepository(Users::class)->list($id, $name, $email, $type, $state, $city, 20, $index * 20);
        $total = $this->em->getRepository(Users::class)->listTotal($id, $name, $email, $type, $state, $city)['total'];
        $partial = ($index * 20) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function candidates(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $post = $request->getQueryParam('post');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $flag = $request->getQueryParam('flag');
        $valid = $request->getQueryParam('valid');
        $users = $this->em->getRepository(Candidates::class)->list($user, $name, $email, $post, $state, $city, $flag, $valid, 20, $index * 20);
        $total = $this->em->getRepository(Candidates::class)->listTotal($user, $name, $email, $post, $state, $city, $flag, $valid)['total'];
        $partial = ($index * 20) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function requirements(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $candidate = $request->getQueryParam('candidate');
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $reason = $request->getQueryParam('reason');
        $requirements = $this->em->getRepository(Requirements::class)->list($candidate, $name, $email, $reason, 20, $index * 20);
        $total = $this->em->getRepository(Requirements::class)->listTotal($candidate, $name, $email, $reason)['total'];
        $partial = ($index * 20) + sizeof($requirements);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $requirements,
            'total' => (int)$total,
            'partial' => $partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function elected(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $year = $request->getQueryParam('year');
        $post = $request->getQueryParam('post');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $elected = $this->em->getRepository(Elected::class)->list($user, $name, $year, $post, $state, $city, 20, $index * 20);
        $total = $this->em->getRepository(Elected::class)->listTotal($user, $name, $year, $post, $state, $city, null)['total'];
        $partial = ($index * 20) + sizeof($elected);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $elected,
            'total' => (int)$total,
            'partial' => $partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }
}