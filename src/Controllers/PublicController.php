<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Services\Email;
use App\Services\Pdf;
use App\Models\Entities\Flags;
use App\Models\Entities\Elected;
use App\Models\Entities\Candidates;
use App\Models\Entities\Requirements;
use App\Models\Entities\Mesoregion;
use App\Models\Entities\Users;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class PublicController extends Controller
{
    private function validToken()
    {
        //$token = 'qj2n50OhznQwzefdshfjsdhfk';
        //$header = apache_request_headers();
        //if (!isset($header['Token']) || $header['Token'] != $token) die('token invalido');
    }

    public function getCandidateList(Request $request, Response $response)
    {
        $this->validToken();
        $candidates = $this->em->getRepository(Candidates::class)->findDetails($data);
        $posts = ['deputadosEstaduais' => [], 'deputadosFederais' => [], 'governadores' => [], 'prefeitos' => [], 'presidente' => [], 'senadores' => [], 'vereadores' => []];
        $candidatesArray = [];
        foreach ($candidates as $c) {
            $candidatesArray[] = ['id' => $c['id'], 'nome' => $c['name'], 'cargo' => $c['post'], 'estado' => $c['state'], 'cidade' => $c['city'], 'legenda' => $c['legend'],
                'foto' => $c['photograph'], 'slug' => $c['slug']];
        }
        $group = array();
        foreach ($candidatesArray as $value) {
            $group[$value["cargo"]][] = $value;
        }
        foreach ($posts as $key => $value) {
            $group[$key] = array_key_exists($key, $group) ? array_merge($group[$key], $value) : $value;
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $group,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getCandidateListDetails(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $candidates = $this->em->getRepository(Candidates::class)->findDetails($data);
        $posts = ['deputadosEstaduais' => [], 'deputadosFederais' => [], 'governadores' => [], 'prefeitos' => [], 'presidente' => [], 'senadores' => [], 'vereadores' => [], 'suplenteDeSenador' => [], 'viceGovernador' => [], 'vicePresidente' => []];
        $candidatesArray = [];
        foreach ($candidates as $c) {
            $candidatesArray[] = ['id' => $c['id'], 'nome' => $c['name'], 'cargo' => $c['post'], 'estado' => $c['state'], 'cidade' => $c['city'], 'legenda' => $c['legend'],
                'foto' => $c['photograph'], 'slug' => $c['slug'], 'banner' => $c['banner']];
        }
        $group = array();
        foreach ($candidatesArray as $value) {
            $group[$value["cargo"]][] = $value;
        }
        foreach ($posts as $key => $value) {
            $group[$key] = array_key_exists($key, $group) ? array_merge($group[$key], $value) : $value;
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $group,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getCandidateListDesignation(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $candidates = $this->em->getRepository(Candidates::class)->findDetailsByDesignation($data);
        $posts = ['Estadual' => [], 'Federal' => [], 'Senado' => [], 'Governo' => [], 'Presidencia' => []];
        $candidatesArray = [];
        foreach ($candidates as $c) {
            $candidatesArray[] = ['id' => $c['id'], 'nome' => $c['name'], 'designacao' => $c['designation'], 'cargo' => $c['post'], 'estado' => $c['state'], 'cidade' => $c['city'], 'legenda' => $c['legend'],
                'foto' => $c['photograph'], 'slug' => $c['slug'], 'banner' => $c['banner']];
        }
        $group = array();
        foreach ($candidatesArray as $value) {
            $group[$value["designacao"]][] = $value;
        }
        foreach ($posts as $key => $value) {
            $group[$key] = array_key_exists($key, $group) ? array_merge($group[$key], $value) : $value;
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $group,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getCandidate(Request $request, Response $response)
    {
        $this->validToken();
        $slug = $request->getAttribute('route')->getArgument('slug');
        $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['slug' => $slug, 'visible' => 1, 'status' => 1]);
        if (!$candidate) throw new \Exception("Nenhum resultado encontrado");
        $requirementDateValid = false;
        if($candidate->getRequirementDate()){
            if($candidate->getRequirementDate()->format('Y-m-d') <= date('Y-m-d')) {
                if(date('Y-m-d', strtotime($candidate->getRequirementDate()->format('Y-m-d') . ' +10 day')) >= date('Y-m-d')){
                    $requirementDateValid = true;
                }
            }
        }
        $candidateArray[] = ['id' => $candidate->getId(), 'nome' => $candidate->getName(), 'email' => $candidate->getEmailCandidate(), 'telefone' => $candidate->getPhone(), 'cargo' => $candidate->getPostString(),
            'estado' => $candidate->getState()->getSigla(), 'cidade' => $candidate->getCity() ? $candidate->getCity()->getCidade() : '', 'profissao' => $candidate->getProfession(), 'cor' => $candidate->getSkinColorString(), 
            'genero' => $candidate->getGender(), 'nomeMarketing' => $candidate->getNameMkt(), 'legenda' => $candidate->getLegend(), 'vaquinha' => $candidate->getCrowdfunding(), 'site' => $candidate->getSite(), 'dataRequerimento' => $candidate->getRequirementDate() ? date('d/m/Y', strtotime($candidate->getRequirementDate()->format('Y-m-d') . ' +10 day')) : '', 'dataRequerimentoValid' => $requirementDateValid, 
            'resumo' => $candidate->getResume(), 'propostas' => $candidate->getProposals(), 'instagram' => $candidate->getInstagram(), 'facebook' => $candidate->getFacebook(), 'linkedin' => $candidate->getLinkedin(), 'twitter' => $candidate->getTwitter(), 'tiktok' => $candidate->getTiktok(), 'youtube' => $candidate->getYoutube(),
            'banco' => $candidate->getBank() ? $candidate->getBank()->getName() : '', 'agencia' => $candidate->getAgency(), 'conta' => $candidate->getAccount(), 'cnpj' => $candidate->getCnpj(), 'foto' => $candidate->getPhotograph(), 'fotoValidada' => $candidate->getPhotographValid(),
            'video' => $candidate->getVideo(), 'videoValidado' => $candidate->getVideoValid(), 'qrCode' => $candidate->getQrCode(), 'slug' => $candidate->getSlug(), 'vice' => $candidate->getVice()
        ];
        return $response->withJson([
            'status' => 'ok',
            'message' => $candidateArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getFlags(Request $request, Response $response)
    {
        $this->validToken();
        $candidate = $request->getAttribute('route')->getArgument('candidato');
        $flags = $this->em->getRepository(Flags::class)->findAll();
        $flagsArray = [];
        foreach ($flags as $f) {
            $flagsArray[] = ['candidato' => $f->getCandidate()->getId(), 'bandeira' => $f->getFlag()->getFlag()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $flagsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getFlagsByCandidate(Request $request, Response $response)
    {
        $this->validToken();
        $slug = $request->getAttribute('route')->getArgument('candidato');
        $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['slug' => $slug]);
        $flags = $this->em->getRepository(Flags::class)->findBy(['candidate' => $candidate->getId()]);
        $flagsArray = [];
        foreach ($flags as $f) {
            $flagsArray[] = ['bandeira' => $f->getFlag()->getFlag()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $flagsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function saveRequirement(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->validToken();
            $target = null;
            $data = (array)$request->getParsedBody();
            $fields = [
                'candidate' => 'Candidato',
                'name' => 'Nome',
                'email' => 'E-mail',
                'cpf' => 'Cpf',
                'phone' => 'Telefone',
                'reason' => 'Motivo',
            ];
            Validator::requireValidator($fields, $data);
            $this->recapcha($data['g-recaptcha-response']);
            $file = $request->getUploadedFiles();
            $file = $file['file'];
            if ($file && $file->getClientFilename()) {
                $extension = explode('.', $file->getClientFilename());
                if ($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
                $target = time() . '.' . end($extension);
                $file->moveTo("uploads/files/" . $target);
            }
            $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['candidate']]);
            $requirement = new Requirements();
            $requirement->setCreated(new \Datetime())
                ->setCandidate($candidate)
                ->setName($data['name'])
                ->setEmail($data['email'])
                ->setCpf(intVal($data['cpf']))
                ->setPhone($data['phone'])
                ->setReason($data['reason'])
                ->setFile($target);
            $this->em->getRepository(Requirements::class)->save($requirement);
            $msg = Messages::newRequirement($candidate);
            Email::send('comite@novo.org.br', '', 'Novo Requerimento de Impugnação -  Candidato ' . $candidate->getName(), $msg);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registrado com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function recapcha(string $recapcha)
    {
        $params = [
            "secret" => "6LctTxIgAAAAANZRV208OWLy3ivvQ0_PobLUEgfA",
            "response" => $recapcha,
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        ];
        $cURL = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($cURL);
        $arr = json_decode($result, true);
        curl_close($cURL);
        if (!$arr['success']) {
            throw new \Exception('Preencha corretamente o capcha');
        }
        return $result;
    }

    public function getCandidateListRequirement(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $candidates = $this->em->getRepository(Candidates::class)->findRequirement($data);
        $posts = ['deputadosEstaduais' => [], 'deputadosFederais' => [], 'governadores' => [], 'prefeitos' => [], 'presidente' => [], 'senadores' => [], 'vereadores' => [], 'SuplenteDeSenador' => [], 'viceGovernador' => [], 'vicePresidente' => []];
        $candidatesArray = [];
        foreach ($candidates as $c) {
            $candidatesArray[] = ['id' => $c['id'], 'nome' => $c['name'], 'cargo' => $c['post'], 'estado' => $c['state'], 'cidade' => $c['city'], 'legenda' => $c['legend'],
                'foto' => $c['photograph'], 'slug' => $c['slug']];
        }
        $group = array();
        foreach ($candidatesArray as $value) {
            $group[$value["cargo"]][] = $value;
        }
        foreach ($posts as $key => $value) {
            $group[$key] = array_key_exists($key, $group) ? array_merge($group[$key], $value) : $value;
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $group,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getMeso(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $fields = ['stateId' => 'Estado'];
        Validator::requireValidator($fields, $data);
        $meso = $this->em->getRepository(Mesoregion::class)->getMeso($data);
        $mesoArray = [];
        foreach ($meso as $m) {
            $mesoArray[] = ['stateId' => $m['stateId'], 'meso' => $m['meso']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $mesoArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getCities(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $fields = ['state' => 'Estado'];
        Validator::requireValidator($fields, $data);
        $cities = $this->em->getRepository(Candidates::class)->getCities($data);
        $citiesArray = [];
        foreach ($cities as $c) {
            $citiesArray[] = ['id' => $c['id'], 'city' => $c['city']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $citiesArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getPosts(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $fields = ['state' => 'Estado'];
        Validator::requireValidator($fields, $data);
        $posts = $this->em->getRepository(Candidates::class)->getPosts($data);
        $postsArray = [];
        foreach ($posts as $p) {
            $postsArray[] = ['id' => $p['id'], 'post' => $p['post']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $postsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getFlagsFilter(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams();
        $fields = ['state' => 'Estado'];
        Validator::requireValidator($fields, $data);
        $flags = $this->em->getRepository(Candidates::class)->getFlagsFilter($data);
        $flagsArray = [];
        foreach ($flags as $f) {
            $flagsArray[] = ['id' => $f['id'], 'flag' => $f['flag']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $flagsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getSuplent(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $fields = ['senator' => 'Senador'];
        Validator::requireValidator($fields, $data);
        $suplents = $this->em->getRepository(Candidates::class)->getSuplents($data);
        $suplentsArray = [];
        foreach ($suplents as $s) {
            $suplentsArray[] = ['id' => $s['id'], 'nome' => $s['name'], 'email' => $s['emailCandidate'], 'telefone' => $s['phone'], 'cargo' => $s['post'],'estado' => $s['state'], 'cidade' => $s['city'], 
                'nomeMarketing' => $s['nameMkt'], 'legenda' => $s['legend'], 'vaquinha' => $s['crowdfunding'], 'site' => $s['site'], 'dataRequerimento' => $s['requirementDate'], 'resumo' => $s['resume'], 
                'propostas' => $s['proposals'], 'instagram' => $s['instagram'], 'facebook' => $s['facebook'], 'linkedin' => $s['linkedin'], 'twitter' => $s['twitter'], 'tiktok' => $s['tiktok'], 'youtube' => $s['youtube'], 
                'banco' => $s['bank'], 'agencia' => $s['agency'], 'conta' => $s['account'], 'cnpj' => $s['cnpj'], 'foto' => $s['photograph'], 'fotoValidada' => $s['photographValid'], 'video' => $s['video'], 
                'videoValid' => $s['videoValid'], 'qrCode' => $s['qrCode'], 'slug' => $s['slug']
            ];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $suplentsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function changeEmail(Request $request, Response $response)
    {
        $this->em->beginTransaction();
        $data = (array)$request->getParams();
        $fields = ['email-antigo' => 'E-mail Antigo', 'email-novo' => 'E-mail Novo'];
        Validator::requireValidator($fields, $data);
        $candidate = $this->em->getRepository(Candidates::class)->findOneBy(['email' => $data['email-antigo']]);
        if($candidate){
            $candidate->setEmail($data['email-novo']);
            $this->em->getRepository(Candidates::class)->save($candidate);
        }
        $user = $this->em->getRepository(Users::class)->findOneBy(['email' => $data['email-antigo']]);
        if($user){
            $user->setEmail($data['email-novo']);
            $this->em->getRepository(Users::class)->save($user);
        }
        $this->em->commit();
    }

    public function saint(Request $request, Response $response)
    {
        $this->em->beginTransaction();
        $data = (array)$request->getParams();
        /* Verifica Dep Estadual */
        $state = null;
        if($data['elementStateModal']){
            $state = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['elementStateModal'], 'visible' => 1, 'status' => 1]);
            if(!$state) throw new \Exception("Solicitação inválida!");
        }
        /* Verifica Dep Federal */
        $federal = null;
        if($data['elementFederalModal']){
            $federal = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['elementFederalModal'], 'visible' => 1, 'status' => 1]);
            if(!$federal) throw new \Exception("Solicitação inválida!");
        }
        /* Verifica Governador*/
        $governor = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['governorModal'], 'visible' => 1, 'status' => 1]);
        if($governor) $governor = strtoupper($governor->getNameMkt());
        /* Verifica Senador*/
        $senator = $this->em->getRepository(Candidates::class)->findOneBy(['id' => $data['senatorModal'], 'visible' => 1, 'status' => 1]);
        if($senator) $senator = strtoupper($senator->getNameMkt());
        Pdf::saint($state, $federal, $governor, $senator);
        $this->em->commit();
    }

    public function saintCandidates(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $fields = ['state' => 'Estado'];
        Validator::requireValidator($fields, $data);
        $candidates = $this->em->getRepository(Candidates::class)->findBy(['state' => $data['state'], 'post' => [1,2], 'visible' => 1, 'status' => 1],['name' => 'ASC']);
        $posts = ['deputadosEstaduais' => [], 'deputadosFederais' => []];
        $candidatesArray = [];
        foreach ($candidates as $c) {
            $candidatesArray[] = ['id' => $c->getId(), 'nome' => strtoupper($c->getNameMkt()), 'cargo' => $c->getPostStringArray(), 'legenda' => $c->getLegend()];
        }
        $group = array();
        foreach ($candidatesArray as $value) {
            $group[$value["cargo"]][] = $value;
        }
        foreach ($posts as $key => $value) {
            $group[$key] = array_key_exists($key, $group) ? array_merge($group[$key], $value) : $value;
        }
        $governor = $this->em->getRepository(Candidates::class)->findOneBy(['post' => 3, 'state' => $data['state'], 'status' => 1, 'visible' => 1]);
        $governor = $governor ? $governor->getId() : '';
        $senator = $this->em->getRepository(Candidates::class)->findOneBy(['post' => 6, 'state' => $data['state'], 'status' => 1, 'visible' => 1]);
        $senator = $senator ? $senator->getId() : '';
        return $response->withJson([
            'status' => 'ok',
            'message' => $group,
            'governor' => $governor,
            'senator' => $senator,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getElectedList(Request $request, Response $response)
    {
        $this->validToken();
        $data = (array)$request->getParams($data); 
        $elected = $this->em->getRepository(Elected::class)->listPublic($data);
        return $response->withJson([
            'status' => 'ok',
            'message' => $elected,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getElected(Request $request, Response $response)
    {
        $this->validToken();
        $slug = $request->getAttribute('route')->getArgument('slug');
        $elected = $this->em->getRepository(Elected::class)->findOneBy(['slug' => $slug, 'active' => 1]);
        if (!$elected) throw new \Exception("Nenhum resultado encontrado");
        $reelected = $elected->getReelected() == 1 ? 'Reeleito(a) em ' : 'Eleito(a) em ';
        $electedArray[] = ['id' => $elected->getId(), 'nome' => $elected->getName(), 'email' => $elected->getEmail(), 'phone' => $elected->getPhone(), 'cargo' => $elected->getPostString(),
            'estado' => $elected->getState()->getSigla(), 'cidade' => $elected->getCity() ? $elected->getCity()->getCidade() : '', 'site' => $elected->getSite(), 'resumo' => $elected->getResume(), 
            'instagram' => $elected->getInstagram(), 'facebook' => $elected->getFacebook(), 'linkedin' => $elected->getLinkedin(), 'twitter' => $elected->getTwitter(), 'tiktok' => $elected->getTiktok(), 
            'youtube' => $elected->getYoutube(), 'slug' => $elected->getSlug(), 'photograph' => $elected->getPhotograph(), 'votes' => $elected->getVotes(), 'year' => $reelected . $elected->getYear()
        ];
        return $response->withJson([
            'status' => 'ok',
            'message' => $electedArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getUfElected(Request $request, Response $response)
    {
        $this->validToken();
        $uf = $request->getAttribute('route')->getArgument('uf');
        $post = $this->em->getRepository(Elected::class)->getUfElected($uf);
        return $response->withJson([
            'status' => 'ok',
            'message' => $post,
        ])->withHeader('Content-Type', 'application/json');
    }
}