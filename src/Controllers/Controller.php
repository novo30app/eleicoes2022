<?php

namespace App\Controllers;
use App\Helpers\Utils;
use App\Models\Entities\Users;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\AccessLog;
use App\Models\Entities\UpdateLogs;
use App\Services\Email;
use Doctrine\ORM\EntityManager;
use App\Helpers\Session;
use Slim\Views\PhpRenderer;

abstract class Controller
{
    protected $em;
    protected $renderer;
    protected $baseUrl = BASEURL;
    protected $env = ENV;

    public function __construct(EntityManager $entityManager, PhpRenderer $renderer)
    {
        $this->em = $entityManager;
        $this->renderer = $renderer;
    }

    protected function getConfigs()
    {
        $config = parse_ini_file('configs.ini', true);
        return $config[$config['environment']];
    }

    protected function getLogged(bool $exception = false)
    {
        $user = Session::get('user');
        if (!$user) {
            if ($exception) throw new \Exception("Você precisa se autenticar");
            Session::set('redirect', $_SERVER["REQUEST_URI"]);
            Session::set('errorMsg', 'Você precisa se autenticar');
            $this->redirect('login');
            exit;
        }
        $user = $this->em->getRepository(Users::class)->find($user);
        return $user;
    }

    protected function redirect(string $url = '')
    {
        header("Location: {$this->baseUrl}{$url}");
        die();
    }

    protected function redirectByPermissions($url = ''){
        Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo.');
        header("Location: {$this->baseUrl}{$url}");
        die();
    }

    protected function newAccessLog(Users $user, $menu): AccessLog
    {
        $acessData = Utils::getAcessData();
        $accessLog = new AccessLog();
        $accessLog->setIp($acessData['ip'])
            ->setDevice($acessData['name'])
            ->setSo($acessData['platform'])
            ->setUser($user);
        return $this->em->getRepository(AccessLog::class)->save($accessLog);
    }

    protected function _retiraAcentos($texto)
    {
        $de = array('Á', 'Í', 'Ó', 'Ú', 'É', 'Ä', 'Ï', 'Ö', 'Ü', 'Ë', 'À', 'Ì', 'Ò', 'Ù', 'È', 'Ã', 'Õ', 'Â', 'Î', 'Ô', 'Û', 'Ê', 'á', 'í', 'ó', 'ú', 'é', 'ä', 'ï', 'ö', 'ü', 'ë', 'à', 'ì', 'ò', 'ù', 'è', 'ã', 'õ', 'â', 'î', 'ô', 'û', 'ê', 'Ç', 'ç');
        $para = array('A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'O', 'A', 'I', 'O', 'U', 'E', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'o', 'a', 'i', 'o', 'u', 'e', 'C', 'c');
        return str_replace($de, $para, $texto);
    }

    protected function saveUpdates($user, $candidate, $data)
    {
        $user = $this->em->getRepository(Users::class)->findOneBy(['id' => $user->getId()]);
        $description = implode(",", $data);
        $update = new UpdateLogs();
        $update->setUser($user)
                ->setCandidate($candidate)
                ->setDescription($description)
                ->setCreated(new \Datetime());
        $this->em->getRepository(UpdateLogs::class)->save($update);
    }
}
