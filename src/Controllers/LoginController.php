<?php

namespace App\Controllers;
use App\Models\Entities\Users;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Candidates;
use App\Models\Entities\RecoverPassword;
use App\Helpers\Session;
use App\Helpers\Validator;
use App\Services\NovoService;
use App\Services\Auth;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class LoginController extends Controller
{
    public function login(Request $request, Response $response)
    {
        return $this->renderer->render($response, 'login/login.phtml');
    }

    public function autentication(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'email' => 'Email',
                'password' => 'Senha',
            ];
            Validator::requireValidator($fields, $data);
            Auth::login($data['email'], $data['password']);
            $user = $this->em->getRepository(Users::class)->findOneBy(['email' => $data['email'], 'active' => 1]);
            if (!$user) throw new \Exception('Usuário ou senha inválidos.');
            if(in_array($user->getType(), array(1, 4))) $this->em->getRepository(Candidates::class)->login($data['email']);
            Session::set('accessType', USER_SESSION);
            Session::set(Session::get('accessType'), $user->getId());
            $this->newAccessLog($user, '');
        } catch (\Exception $e) {
            Session::set('errorMsg', $e->getMessage());
            header("Refresh: 0");
            exit;
        }
        $this->redirect();
    }

    public function logout(Request $request, Response $response)
    {
        Session::forgot(Session::get('accessType'));
        header("Location: {$this->baseUrl}/login");
        exit;
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $recover = $this->em->getRepository(RecoverPassword::class)->findOneBy(['token' => $id, 'used' => 0]);
            if ($recover) {
                $recover->setUsed(1);
                $this->em->getRepository(RecoverPassword::class)->save($recover);
                $user = $this->em->getRepository(Users::class)->findOneBy(['id' => $recover->getUser()->getId()]);
                $user->setActive(1);
                $this->em->getRepository(Users::class)->save($user);
            }
            $this->em->commit();
        } catch (\Exception $e) {
            Session::set('errorMsg', $e->getMessage());
            header("Refresh: 0");
            exit;
        }
        $this->redirect();
    }
}