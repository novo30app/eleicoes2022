<?php

namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="states")
 */
class State
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @var string|null
     * @Column(name="estado", type="string", length=45, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     * @Column(name="sigla", type="string", length=3, nullable=true)
     */
    private $sigla;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): State
    {
        $this->id = $id;
        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): State
    {
        $this->estado = $estado;
        return $this;
    }

    public function getSigla(): ?string
    {
        return $this->sigla;
    }

    public function setSigla(?string $sigla): State
    {
        $this->sigla = $sigla;
        return $this;
    }
}
