<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="banner")
 * @ORM @Entity(repositoryClass="App\Models\Repository\BannerRepository")
 */
class Banner
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var Candidates
     *
     * @ManyToOne(targetEntity="Candidates")
     * @JoinColumns({
     *   @JoinColumn(name="candidate", referencedColumnName="id")
     * })
     */
    private $candidate;

    /**
     * @Column(type="string")
     * @var string
     *      */
    private $banner;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCandidate(): Candidates
    {
        return $this->candidate;
    }

    public function setCandidate(Candidates $candidate): Banner
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getBanner(): Banner
    {
        return $this->banner;
    }

    public function setBanner(Banner $banner): Banner
    {
        $this->banner = $banner;
        return $this;
    }
}