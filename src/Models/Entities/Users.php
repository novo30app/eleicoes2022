<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="users")
 * @ORM @Entity(repositoryClass="App\Models\Repository\UsersRepository")
 */
class Users
{
    const TYPE_CANDIDATE = 1;
    const TYPE_ADMIN = 2;
    const TYPE_MANAGER = 3;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $type = 1;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string", unique=true)
     */
    private string $email = '';

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): Users
    {
        $this->type = $type;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Users
    {
        $this->active = $active;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Users
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Users
    {
        $this->email = $email;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): Users
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): Users
    {
        $this->state = $state;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): Users
    {
        $this->created = $created;
        return $this;
    }
}