<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="elected")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ElectedRepository")
 */
class Elected
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $year;

    /**
     * @Column(type="boolean")
     */
    private bool $reelected = false;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $post;

    /**
     * @Column(type="string")
     * @var string
     */
    private $votes;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     * @var City
     */
    private $city;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     * @var State
     */
    private $state;

    /**
     * @Column(type="string")
     * @var string
     */
    private $resume;

    /**
     * @Column(type="string")
     * @var string
     */
    private $instagram;

    /**
     * @Column(type="string")
     * @var string
     */
    private $facebook;

    /**
     * @Column(type="string")
     * @var string
     */
    private $linkedin;

    /**
     * @Column(type="string")
     * @var string
     */
    private $twitter;

    /**
     * @Column(type="string")
     * @var string
     */
    private $tiktok;

    /**
     * @Column(type="string")
     * @var string
     */
    private $youtube;

    /**
     * @Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(type="string")
     * @var string
     */
    private $site;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phone;

    /**
     * @Column(type="string")
     * @var string
     */
    private $slug;

    /**
     * @Column(type="string")
     * @var string
     */
    private $photograph;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): Elected
    {
        $this->created = $created;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Elected
    {
        $this->active = $active;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Elected
    {
        $this->name = $name;
        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): Elected
    {
        $this->year = $year;
        return $this;
    }

    public function getReelected(): bool
    {
        return $this->reelected;
    }

    public function setReelected(bool $reelected): Elected
    {
        $this->reelected = $reelected;
        return $this;
    }

    public function getPost(): int
    {
        return $this->post;
    }

    public function getPostString(): ?string
    {
        switch ($this->post) {
            case 1:
                $post = 'Deputado(a) Estadual/Distrital';
                break;
            case 2:
                $post = 'Deputado(a) Federal';
                break;           
            case 3:
                $post = 'Governador(a)';
                break;            
            case 4:
                $post = 'Prefeito';
                break;
            case 5:
                $post = 'Presidente';
                break;
            case 6:
                $post = 'Senador';
                break;
            case 7:
                $post = 'Vereador';
                break;
            case 8:
                $post = 'Suplente de Senador(a)';
                break;
            case 9:
                $post = 'Vice Governador(a)';
                break;
            case 10:
                $post = 'Vice Presidente';
                break;
            case 11:
                $post = 'Vice Prefeito(a)';
                break;
            default:
                $post = '-';
                break;
        }
        return $post;
    }

    public function setPost(int $post): Elected
    {
        $this->post = $post;
        return $this;
    }

    public function getVotes(): string
    {
        return $this->votes;
    }

    public function setVotes(string $votes): Elected
    {
        $this->votes = $votes;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): Elected
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Elected
    {
        $this->state = $state;
        return $this;
    }

    public function getResume(): string
    {
        return $this->resume;
    }

    public function setResume(string $resume): Elected
    {
        $this->resume = $resume;
        return $this;
    }

    public function getInstagram(): string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): Elected
    {
        $this->instagram = $instagram;
        return $this;
    }

    public function getFacebook(): string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): Elected
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function getLinkedin(): string
    {
        return $this->linkedin;
    }

    public function setLinkedin(string $linkedin): Elected
    {
        $this->linkedin = $linkedin;
        return $this;
    }

    public function getTwitter(): string
    {
        return $this->twitter;
    }

    public function setTwitter(string $twitter): Elected
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function getTiktok(): string
    {
        return $this->tiktok;
    }

    public function setTiktok(string $tiktok): Elected
    {
        $this->tiktok = $tiktok;
        return $this;
    }

    public function getYoutube(): string
    {
        return $this->youtube;
    }

    public function setYoutube(string $youtube): Elected
    {
        $this->youtube = $youtube;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Elected
    {
        $this->email = $email;
        return $this;
    }

    public function getSite(): string
    {
        return $this->site;
    }

    public function setSite(string $site): Elected
    {
        $this->site = $site;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): Elected
    {
        $this->phone = $phone;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): Elected
    {
        $this->slug = $slug;
        return $this;
    }

    public function getPhotograph(): string
    {
        return $this->photograph;
    }

    public function setPhotograph(string $photograph): Elected
    {
        $this->photograph = $photograph;
        return $this;
    }
}