<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="updateLogs")
 * @ORM @Entity(repositoryClass="App\Models\Repository\UpdateLogsRepository")
 */
class UpdateLogs
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Users")
     * @JoinColumn(name="user", referencedColumnName="id")
     * @var Users
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="Candidates")
     * @JoinColumn(name="candidate", referencedColumnName="id")
     * @var Candidates
     */
    private $candidate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $description;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): UpdateLogs
    {
        return $this->user;
    }

    public function setUser(Users $user): UpdateLogs
    {
        $this->user = $user;
        return $this;
    }

    public function getCadidate(): UpdateLogs
    {
        return $this->candidate;
    }

    public function setCandidate(Candidates $candidate): UpdateLogs
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): UpdateLogs
    {
        $this->description = $description;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): UpdateLogs
    {
        $this->created = $created;
        return $this;
    }
}