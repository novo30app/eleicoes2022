<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="flags")
 * @ORM @Entity(repositoryClass="App\Models\Repository\FlagsRepository")
 */
class Flags
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var Candidates
     *
     * @ManyToOne(targetEntity="Candidates")
     * @JoinColumns({
     *   @JoinColumn(name="candidate", referencedColumnName="id")
     * })
     */
    private $candidate;

    /**
     * @var Candidates
     *
     * @ManyToOne(targetEntity="FlagsList")
     * @JoinColumns({
     *   @JoinColumn(name="flag", referencedColumnName="id")
     * })
     */
    private $flag;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCandidate(): Candidates
    {
        return $this->candidate;
    }

    public function setCandidate(Candidates $candidate): Flags
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getFlag(): FlagsList
    {
        return $this->flag;
    }

    public function setFlag(FlagsList $flag): Flags
    {
        $this->flag = $flag;
        return $this;
    }
}