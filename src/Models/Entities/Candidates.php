<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="candidates")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CandidatesRepository")
 */
class Candidates
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(type="string")
     * @var string
     */
    private $emailCandidate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phone;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $post;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $designation;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     * @var State
     */
    private $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     * @var City
     */
    private $city;

    /**
     * @Column(type="string")
     * @var string
     */
    private $profession;
    
    /**
     * @Column(type="integer")
     * @var int
     */
    private $skinColor;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $gender;

    /**
     * @Column(type="string")
     * @var string
     */
    private $nameMkt;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $legend;

    /**
     * @Column(type="string")
     * @var string
     */
    private $crowdfunding;

    /**
     * @Column(type="string")
     * @var string
     */
    private $site;

    /**
     * @Column(type="string")
     * @var string
     */
    private $resume;

    /**
     * @Column(type="string")
     * @var string
     */
    private $proposals;

    /**
     * @Column(type="string")
     * @var string
     */
    private $instagram;

    /**
     * @Column(type="string")
     * @var string
     */
    private $facebook;

    /**
     * @Column(type="string")
     * @var string
     */
    private $linkedin;

    /**
     * @Column(type="string")
     * @var string
     */
    private $twitter;

    /**
     * @Column(type="string")
     * @var string
     */
    private $tiktok;

    /**
     * @Column(type="string")
     * @var string
     */
    private $youtube;

    /**
     * @Column(type="string")
     * @var string
     */
    private $cnpj;

    /**
     * @ManyToOne(targetEntity="Banks")
     * @JoinColumn(name="bank", referencedColumnName="id")
     * @var Banks
     */
    private $bank;

    /**
     * @Column(type="string")
     * @var string
     */
    private $agency;

    /**
     * @Column(type="string")
     * @var string
     */
    private $account;

    /**
     * @Column(type="string")
     * @var string
     */
    private $photograph;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $photographValid;

    /**
     * @Column(type="string")
     * @var string
     */
    private $video;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $videoValid;

    /**
     * @Column(type="string")
     * @var string
     */
    private $qrCode;

    /**
     * @Column(type="datetime", nullable=true,)
     */
    private ?\DateTime $requirementDate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $slug;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $visible;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $status;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $term;

    /**
     * @Column(type="string")
     * @var string
     */
    private $vice;

    /**
     * @Column(type="string")
     * @var string
     */
    private $obs;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): Candidates
    {
        $this->created = $created;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Candidates
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): Candidates
    {
        $this->email = $email;
        return $this;
    }

    public function getEmailCandidate(): ?string
    {
        return $this->emailCandidate;
    }

    public function setEmailCandidate(string $emailCandidate): Candidates
    {
        $this->emailCandidate = $emailCandidate;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): Candidates
    {
        $this->phone = $phone;
        return $this;
    }

    public function getPost(): ?int
    {
        return $this->post;
    }

    public function getPostString(): ?string
    {
        switch ($this->post) {
            case 1:
                $post = 'Deputado(a) Estadual/Distrital';
                break;
            case 2:
                $post = 'Deputado(a) Federal';
                break;           
            case 3:
                $post = 'Governador(a)';
                break;            
            case 4:
                $post = 'Prefeito';
                break;
            case 5:
                $post = 'Presidente';
                break;
            case 6:
                $post = 'Senador';
                break;
            case 7: 
                $post = 'Vereador';
                break;
            case 8: 
                $post = 'Suplente de Senador(a)';
                break;   
            case 9: 
                $post = 'Vice Governador(a)';
                break;
            case 10: 
                $post = 'Vice Presidente';
                break;   
            default:
                $post = '-';
                break;
        }
        return $post;
    }

    public function getPostStringArray(): ?string
    {
        switch ($this->post) {
            case 1:
                $post = 'deputadosEstaduais';
                break;
            case 2:
                $post = 'deputadosFederais';
                break;           
            case 3:
                $post = 'governador';
                break;            
            case 4:
                $post = 'prefeito';
                break;
            case 5:
                $post = 'presidente';
                break;
            case 6:
                $post = 'senador';
                break;
            case 7: 
                $post = 'vereador';
                break;
            case 8: 
                $post = 'suplenteDeSenado';
                break;   
            case 9: 
                $post = 'viceGovernado';
                break;
            case 10: 
                $post = 'vicePresidente';
                break;   
            default:
                $post = '-';
                break;
        }
        return $post;
    }

    public function setPost(int $post): Candidates
    {
        $this->post = $post;
        return $this;
    }

    public function getDesignation(): ?int
    {
        return $this->designation;
    }

    public function getDesignationString(): ?string
    {
        switch ($this->designation) {
            case 1:
                $designation = 'Estadual/Distrital';
                break;
            case 2:
                $designation = 'Federal';
                break;           
            case 3:
                $designation = 'Senado';
                break;            
            case 4:
                $designation = 'Governo';
                break;
            case 5:
                $designation = 'Presidencia';
                break;
        }
        return $designation;
    }

    public function setDesignation(int $designation): Candidates
    {
        $this->designation = $designation;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Candidates
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): Candidates
    {
        $this->city = $city;
        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(string $profession): Candidates
    {
        $this->profession = $profession;
        return $this;
    }

    public function getSkinColor(): ?int
    {
        return $this->skinColor;
    }

    public function getSkinColorString(): ?string
    {
        switch ($this->skinColor) {
            case 1:
                $skinColor = 'Branca';
                break;
            case 2:
                $skinColor = 'Preta';
                break;           
            case 3:
                $skinColor = 'Parda';
                break;            
            case 4:
                $skinColor = 'Amarela';
                break;            
            case 5:
                $skinColor = 'Indígena';
                break;            
            default:
                $skinColor = '-';
                break;
        }
        return $skinColor;
    }

    public function setSkinColor(int $skinColor): Candidates
    {
        $this->skinColor = $skinColor;
        return $this;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function getGenderString(): ?string
    {
        switch ($this->gender) {
            case 1:
                $gender = 'Masculino';
                break;
            case 2:
                $gender = 'Feminino';
                break;                         
            default:
                $gender = '-';
                break;
        }
        return $gender;
    }

    public function setGender(int $gender): Candidates
    {
        $this->gender = $gender;
        return $this;
    }

    public function getNameMkt(): ?string
    {
        return $this->nameMkt;
    }

    public function setNameMkt(string $nameMkt): Candidates
    {
        $this->nameMkt = $nameMkt;
        return $this;
    }

    public function getLegend(): ?int
    {
        return $this->legend;
    }

    public function setLegend(int $legend): Candidates
    {
        $this->legend = $legend;
        return $this;
    }

    public function getCrowdfunding(): ?string
    {
        return $this->crowdfunding;
    }

    public function setCrowdfunding(string $crowdfunding): Candidates
    {
        $this->crowdfunding = $crowdfunding;
        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(string $site): Candidates
    {
        $this->site = $site;
        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): Candidates
    {
        $this->resume = $resume;
        return $this;
    }

    public function getProposals(): ?string
    {
        return $this->proposals;
    }

    public function setProposals(string $proposals): Candidates
    {
        $this->proposals = $proposals;
        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): Candidates
    {
        $this->instagram = $instagram;
        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): Candidates
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(string $linkedin): Candidates
    {
        $this->linkedin = $linkedin;
        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(string $twitter): Candidates
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function getTiktok(): ?string
    {
        return $this->tiktok;
    }

    public function setTiktok(string $tiktok): Candidates
    {
        $this->tiktok = $tiktok;
        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(string $youtube): Candidates
    {
        $this->youtube = $youtube;
        return $this;
    }

    public function getBank(): ?Banks
    {
        return $this->bank;
    }

    public function setBank(Banks $bank): Candidates
    {
        $this->bank = $bank;
        return $this;
    }

    public function getAgency(): ?string
    {
        return $this->agency;
    }

    public function setAgency(string $agency): Candidates
    {
        $this->agency = $agency;
        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(string $account): Candidates
    {
        $this->account = $account;
        return $this;
    }

    public function getPhotographPending(): ?string
    {
        if($this->photograph == null){
            if($this->gender == 2){
                $photograph = 'gender_2.jpeg';
            } else {
                $photograph = 'gender_1.jpeg';
            }
        } else {
            $photograph = $this->photograph;
        }
        return $photograph;
    }

    public function getPhotograph(): ?string
    {
        if($this->photograph == null || $this->photographValid != 1){
            if($this->gender == 2){
                $photograph = 'gender_2.jpeg';
            } else {
                $photograph = 'gender_1.jpeg';
            }
        } else {
            $photograph = $this->photograph;
        }
        return $photograph;
    }

    public function setPhotograph(string $photograph): Candidates
    {
        $this->photograph = $photograph;
        return $this;
    }

    public function getPhotographValid(): ?int
    {
        return $this->photographValid;
    }

    public function setPhotographValid(int $photographValid): Candidates
    {
        $this->photographValid = $photographValid;
        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): Candidates
    {
        $this->video = $video;
        return $this;
    }

    public function getVideoValid(): ?int
    {
        return $this->videoValid;
    }

    public function setVideoValid(int $videoValid): Candidates
    {
        $this->videoValid = $videoValid;
        return $this;
    }  
    
    public function getQrCode(): ?string
    {
        return $this->qrCode;
    }

    public function setQrCode(string $qrCode): Candidates
    {
        $this->qrCode = $qrCode;
        return $this;
    }

    public function getRequirementDate()
    {
        return $this->requirementDate;
    }

    public function setRequirementDate(?\DateTime $requirementDate): Candidates
    {
        $this->requirementDate = $requirementDate;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): Candidates
    {
        $this->slug = $slug;
        return $this;
    }

    public function getVisible(): ?int
    {
        return $this->visible;
    }

    public function setVisible(int $visible): Candidates
    {
        $this->visible = $visible;
        return $this;
    }  

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): Candidates
    {
        $this->status = $status;
        return $this;
    }  

    public function getTerm(): ?int
    {
        return $this->term;
    }

    public function setTerm(int $term): Candidates
    {
        $this->term = $term;
        return $this;
    }  

    public function getVice(): ?string
    {
        return $this->vice;
    }

    public function setVice(string $vice): Candidates
    {
        $this->vice = $vice;
        return $this;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(string $cnpj): Candidates
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    public function getObs(): ?string
    {
        return $this->obs;
    }

    public function setObs(string $obs): Candidates
    {
        $this->obs = $obs;

        return $this;
    }
}