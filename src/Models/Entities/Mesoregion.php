<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="mesoregion")
 * @ORM @Entity(repositoryClass="App\Models\Repository\MesoregionRepository")
 */
class Mesoregion
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var City
     * @ManyToOne(targetEntity="City")
     * @JoinColumns({
     *   @JoinColumn(name="cityId", referencedColumnName="id")
     * })
     */
    private $cityId;

    /**
     * @Column(type="string")
     * @var string
     */
    private $city;

    /**
     * @var State
     * @ManyToOne(targetEntity="State")
     * @JoinColumns({
     *   @JoinColumn(name="stateId", referencedColumnName="id")
     * })
     */
    private $stateId;

    /**
     * @Column(type="string")
     * @var string
     */
    private $state;

    /**
     * @Column(type="string")
     * @var string
     */
    private $meso;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCityId(): City
    {
        return $this->cityId;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getStateId(): State
    {
        return $this->stateId;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getMeso(): string
    {
        return $this->meso;
    }
}