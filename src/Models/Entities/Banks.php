<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="banks")
 */
class Banks
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $cod = '';

    /**
     * @Column(type="string")
     */
    private string $name = '';

    public function __toString(){
        return $this->name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCod(): string
    {
        return $this->cod;
    }

    public function setCod(string $cod): Banks
    {
        $this->cod = $cod;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Banks
    {
        $this->name = $name;
        return $this;
    }
}