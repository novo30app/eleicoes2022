<?php

namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="cities")
 */
class City
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @var string|null
     * @Column(name="cidade", type="string", length=60, nullable=true)
     */
    private $cidade;

    /**
     * @var bool|null
     * @Column(name="capital", type="boolean", nullable=true)
     */
    private $capital;

    /**
     * @var State
     *
     * @ManyToOne(targetEntity="State")
     * @JoinColumns({
     *   @JoinColumn(name="tb_estado_id", referencedColumnName="id")
     * })
     */
    private $state;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return City
     */
    public function setId(int $id): City
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCidade(): ?string
    {
        return $this->cidade;
    }

    /**
     * @param string|null $cidade
     * @return City
     */
    public function setCidade(?string $cidade): City
    {
        $this->cidade = $cidade;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCapital(): ?bool
    {
        return $this->capital;
    }

    /**
     * @param bool|null $capital
     * @return City
     */
    public function setCapital(?bool $capital): City
    {
        $this->capital = $capital;
        return $this;
    }

    /**
     * @return State
     */
    public function getState(): State
    {
        return $this->state;
    }

    /**
     * @param State $state
     * @return City
     */
    public function setState(State $state): City
    {
        $this->state = $state;
        return $this;
    }
}
