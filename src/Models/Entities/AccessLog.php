<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="accesslog")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AccessLogRepository")
 */
class AccessLog
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Users")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     * @var Users
     */
    private $user;

    /**
     * @Column(type="string")
     * @var string
     */
    private $ip;

    /**
     * @Column(type="string")
     * @var string
     */
    private $device;

    /**
     * @Column(type="string")
     * @var string
     */
    private $so;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): Users
    {
        return $this->user;
    }

    public function setUser(Users $user): AccessLog
    {
        $this->user = $user;
        return $this;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): AccessLog
    {
        $this->ip = $ip;
        return $this;
    }

    public function getDevice(): string
    {
        return $this->device;
    }

    public function setDevice(string $device): AccessLog
    {
        $this->device = $device;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getSo(): string
    {
        return $this->so;
    }

    public function setSo(string $so): AccessLog
    {
        $this->so = $so;
        return $this;
    }
}