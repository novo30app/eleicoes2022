<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="recoverPassword")
 * @ORM @Entity(repositoryClass="App\Models\Repository\RecoverPasswordRepository")
 */
class RecoverPassword
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Users")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     * @var Users
     */
    private $user;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Column(type="string")
     * @var string
     */
    private $token;

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $used;

    /**
     * @ManyToOne(targetEntity="Users")
     * @JoinColumn(name="userAdmin", referencedColumnName="id", nullable=true)
     * @var Users
     */
    private $userAdmin;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): Users
    {
        return $this->user;
    }

    public function setUser(Users $user): RecoverPassword
    {
        $this->user = $user;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): RecoverPassword
    {
        $this->created = $created;
        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): RecoverPassword
    {
        $this->token = $token;
        return $this;
    }

    public function isUsed(): bool
    {
        return $this->used;
    }

    public function setUsed(bool $used): RecoverPassword
    {
        $this->used = $used;
        return $this;
    }

    public function getUserAdmin(): Users
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(Users $userAdmin): RecoverPassword
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }
}