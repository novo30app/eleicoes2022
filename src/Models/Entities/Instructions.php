<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="instructions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InstructionsRepository")
 */
class Instructions
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string")
     * @var string
     */
    private $instruction;

    /**
     * @ManyToOne(targetEntity="Users")
     * @JoinColumn(name="user", referencedColumnName="id")
     * @var Users
     */
    private $user;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $modified;

    public function getId(): int
    {
        return $this->id;
    }

    public function getInstruction(): ?string
    {
        return $this->instruction;
    }

    public function setInstruction(string $instruction): Instructions
    {
        $this->instruction = $instruction;
        return $this;
    }

    public function getUser(): Users
    {
        return $this->user;
    }

    public function setUser(Users $user): Instructions
    {
        $this->user = $user;
        return $this;
    }

    public function getModiFied(): \DateTime
    {
        return $this->modified;
    }

    public function setModiFied(\DateTime $modified): Instructions
    {
        $this->modified = $modified;
        return $this;
    }
}
