<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="flagsList")
 * @ORM @Entity(repositoryClass="App\Models\Repository\FlagsListRepository")
 */
class FlagsList
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     * @var string
     */
    private $flag;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFlag(): string
    {
        return $this->flag;
    }
}