<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="requirements")
 * @ORM @Entity(repositoryClass="App\Models\Repository\RequirementsRepository")
 */
class Requirements
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="Candidates")
     * @JoinColumn(name="candidate", referencedColumnName="id")
     * @var Candidates
     */
    private $candidate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(type="string")
     * @var int
     */
    private $cpf;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phone;

    /**
     * @Column(type="string")
     * @var string
     */
    private $reason;

    /**
     * @Column(type="string")
     * @var string
     */
    private $file;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): Requirements
    {
        $this->created = $created;
        return $this;
    }

    public function getCadidate(): Requirements
    {
        return $this->candidate;
    }

    public function setCandidate(Candidates $candidate): Requirements
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Requirements
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): Requirements
    {
        $this->email = $email;
        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): Requirements
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): Requirements
    {
        $this->phone = $phone;
        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(string $reason): Requirements
    {
        $this->reason = $reason;
        return $this;
    }
    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): Requirements
    {
        $this->file = $file;
        return $this;
    }
}