<?php

namespace App\Models\Repository;
use App\Models\Entities\Elected;
use Doctrine\ORM\EntityRepository;

class ElectedRepository extends EntityRepository
{
    public function save(Elected $entity):Elected
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT 20 OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, $year = null, $post = null, $state = null, $city = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND e.name LIKE :name";
        }
        if ($year) {
            $params[':year'] = "$year";
            $where .= " AND e.year = :year";
        }
        if ($post) {
            $params[':post'] = $post;
            $where .= " AND e.post = :post";
        }    
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND e.state = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND e.city = :city";
        }  
        return $where;
    }

    public function list($user = null, $name = null, $year = null, $post = null, $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $year, $post, $state, $city, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT e.id, e.name, e.year, s.estado AS state, IFNULL(c.cidade, '-') AS city, e.slug, e.photograph,
                (CASE e.post
                    WHEN 1 THEN 'Deputado(a) Estadual/Distrital'
                    WHEN 2 THEN 'Deputado(a) Federal'
                    WHEN 3 THEN 'Governador(a)'
                    WHEN 4 THEN 'Prefeito'
                    WHEN 5 THEN 'Presidente'
                    WHEN 6 THEN 'Senador'
                    WHEN 7 THEN 'Vereador'
                    WHEN 8 THEN 'Suplente de Senador(a)'
                    WHEN 9 THEN 'Vice Governador(a)'
                    WHEN 10 THEN 'Vice Presidente'
                    WHEN 11 THEN 'Vice Prefeito(a)'
                    ELSE ''
                END) AS post
                FROM elected e
                LEFT JOIN states s ON s.id = e.state
                LEFT JOIN cities c ON c.id = e.city
                WHERE 1 = 1 {$where} 
                GROUP BY e.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($user = null, $name = null, $year = null, $post = null, $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $year, $post, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(*)) AS total
                FROM elected e
                WHERE e.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listPublic($data): array
    {
        $where = '';
        if ($data['state']) {
            $params[':state'] = $data['state'];
            $where .= " AND e.state = :state";
        }
        if ($data['post']) {
            $params[':post'] = $data['post'];
            $where .= " AND e.post = :post";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT e.id, e.name, s.estado AS state, IFNULL(c.cidade, '-') AS city, e.slug, e.photograph,
                CONCAT(IF(e.reelected = 1, 'Reeleito(a) em ', 'Eleito(a) em '), e.year) AS year,
                (CASE e.post
                    WHEN 1 THEN 'Deputado(a) Estadual/Distrital'
                    WHEN 2 THEN 'Deputado(a) Federal'
                    WHEN 3 THEN 'Governador(a)'
                    WHEN 4 THEN 'Prefeito'
                    WHEN 5 THEN 'Presidente'
                    WHEN 6 THEN 'Senador'
                    WHEN 7 THEN 'Vereador(a)'
                    WHEN 8 THEN 'Suplente de Senador(a)'
                    WHEN 9 THEN 'Vice Governador(a)'
                    WHEN 10 THEN 'Vice Presidente'
                    WHEN 11 THEN 'Vice Prefeito(a)'
                    ELSE ''
                END) AS post
                FROM elected e
                LEFT JOIN states s ON s.id = e.state
                LEFT JOIN cities c ON c.id = e.city
                WHERE e.active = 1 {$where}
                GROUP BY e.id
                ORDER BY FIELD(e.post, 5, 3, 9, 4, 11, 6, 8, 2, 1, 7), e.state";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function getUfElected($uf)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT e.post AS postId,
                (CASE e.post
                    WHEN 1 THEN 'Deputado(a) Estadual/Distrital'
                    WHEN 2 THEN 'Deputado(a) Federal'
                    WHEN 3 THEN 'Governador(a)'
                    WHEN 4 THEN 'Prefeito'
                    WHEN 5 THEN 'Presidente'
                    WHEN 6 THEN 'Senador'
                    WHEN 7 THEN 'Vereador(a)'
                    WHEN 8 THEN 'Suplente de Senador(a)'
                    WHEN 9 THEN 'Vice Governador(a)'
                    WHEN 10 THEN 'Vice Presidente'
                    WHEN 11 THEN 'Vice Prefeito(a)'
                    ELSE ''
                END) AS post
                FROM elected e WHERE e.active = 1 AND e.state = :uf GROUP BY e.post";
        $rows = $pdo->prepare($sql)->execute([':uf' => $uf]);
        return $rows->fetchAllAssociative();
    }
}