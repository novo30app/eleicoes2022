<?php

namespace App\Models\Repository;
use App\Models\Entities\UpdateLogs;
use Doctrine\ORM\EntityRepository;

class UpdateLogsRepository extends EntityRepository
{
    public function save(UpdateLogs $entity):UpdateLogs
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}