<?php

namespace App\Models\Repository;
use App\Models\Entities\FlagsList;
use Doctrine\ORM\EntityRepository;

class FlagsListRepository extends EntityRepository
{
    public function save(FlagsList $entity):FlagsList
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function list()
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM flagsList WHERE id != :id ORDER BY flag ASC";
        $rows = $pdo->prepare($sql)->execute([':id' => 24]);
        return $rows->fetchAllAssociative();
    }
}