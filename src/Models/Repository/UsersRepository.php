<?php

namespace App\Models\Repository;
use App\Models\Entities\Users;
use Doctrine\ORM\EntityRepository;

class UsersRepository extends EntityRepository
{
    public function save(Users $entity): Users
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function login(string $email)
    {
        $user = $this->findOneBy(['email' => $email, 'active' => 1]);
        if (!$user) {
            throw new \Exception('Usuário ou senha inválidos.');
        }
        return $user;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($id = null, $name = null, $email = null, $type = null, $state = null, $city = null, &$params): string
    {
        $where = '';
        if($id) {
            $params[':id'] = "$id";
            $where .= " AND u.id = :id";
        }
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND u.name LIKE :name";
        }
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND u.email LIKE :email";
        }
        if ($type) {
            $params[':type'] = $type;
            $where .= " AND u.type = :type";
        }    
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND c.id = :city";
        }  
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND s.id = :state";
        }  
        return $where;
    }

    public function list($id = null, $name = null, $email = null, $type = null, $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($id, $name, $email, $type, $state, $city, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT u.id, u.name, u.email, u.type, 
                        (CASE u.type
                            WHEN 1 THEN 'Candidato'
                            WHEN 2 THEN 'Administrador'
                            WHEN 3 THEN 'Dirigente'
                            WHEN 4 THEN 'Coordenador'
                        END) AS 'typeString', 
                 u.active, s.id AS 'stateId', s.estado AS 'state', c.id AS 'cityId', IFNULL(c.cidade, '-') AS 'city'
                FROM eleicoes2022.users u
                LEFT JOIN states s ON s.id = u.state
                LEFT JOIN cities c ON c.id = u.city
                WHERE 1 = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($id = null, $name = null, $email = null, $type = null, $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($id, $name, $email, $type, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(*)) AS 'total' 
                FROM eleicoes2022.users u
                LEFT JOIN states s ON s.id = u.state
                LEFT JOIN cities c ON c.id = u.city
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}