<?php

namespace App\Models\Repository;
use App\Models\Entities\Instructions;
use Doctrine\ORM\EntityRepository;

class InstructionsRepository extends EntityRepository
{
    public function save(Instructions $entity):Instructions
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}