<?php

namespace App\Models\Repository;
use App\Models\Entities\Requirements;
use Doctrine\ORM\EntityRepository;

class RequirementsRepository extends EntityRepository
{
    public function save(Requirements $entity): Requirements
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($candidate = null, $name = null, $email = null, $reason = null, &$params): string
    {
        $where = '';
        if ($candidate) {
            $params[':candidate'] = "$candidate";
            $where .= " AND r.candidate = :candidate";
        }
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND r.name LIKE :name";
        }
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND r.email LIKE :email";
        }
        if ($reason) {
            $params[':reason'] = "%$reason%";
            $where .= " AND r.reason LIKE :reason";
        }
        return $where;
    }

    public function list($candidate = null, $name = null, $email = null, $reason = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($candidate, $name, $email, $reason, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT r.id, DATE_FORMAT(r.created, '%d/%m/%Y') AS created, c.name AS candidate, r.name, r.email, r.cpf, r.phone, r.reason, r.file
                FROM eleicoes2022.requirements r
                LEFT JOIN eleicoes2022.candidates c ON c.id = r.candidate
                WHERE 1 = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($candidate = null,$name = null, $email = null, $reason = null): array
    {
        $params = [];
        $where = $this->generateWhere($candidate, $name, $email, $reason, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(*)) AS total
                FROM eleicoes2022.requirements r
                LEFT JOIN eleicoes2022.candidates c ON c.id = r.candidate
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}