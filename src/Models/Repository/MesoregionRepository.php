<?php

namespace App\Models\Repository;
use App\Models\Entities\Mesoregion;
use Doctrine\ORM\EntityRepository;

class MesoregionRepository extends EntityRepository
{
    public function save(Mesoregion $entity):Mesoregion
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getMeso($data)
    {
        $params = [];
        $where = '';
        if ($data['gender']) {
            $params[':gender'] = $data['gender'];
            $where .= " AND gender = :gender";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM eleicoes2022.mesoregion 
                WHERE stateId = {$data['stateId']} AND cityId IN (SELECT city FROM eleicoes2022.candidates WHERE status = 1 AND visible = 1 {$where})
                GROUP BY eleicoes2022.mesoregion.meso";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}