<?php

namespace App\Models\Repository;
use App\Models\Entities\Candidates;
use Doctrine\ORM\EntityRepository;

class CandidatesRepository extends EntityRepository
{
    public function save(Candidates $entity): Candidates
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function login(string $email)
    {
        $candidate = $this->findOneBy(['email' => $email]);
        if (!$candidate) {
            throw new \Exception('Registro não encontrado.');
        }
        return $candidate;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, $email = null, $post = null, $state = null, $city = null, $flag = null, $valid = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND (c.nameMkt LIKE :name || c.name LIKE :name)";
        }
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND c.email LIKE :email";
        }
        if ($post) {
            $params[':post'] = $post;
            $where .= " AND c.post = :post";
        }    
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND c.city = :city";
        }  
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND c.state = :state";
        } 
        if($user->getType() == 3){
            $params[':state'] = $user->getState()->getId();
            $where .= " AND c.state = :state";
        }
        if($valid){
            if($valid == 1){
                $where .= " AND photograph IS NOT NULL AND photographValid = 1";
            } else if ($valid == 2){
                $where .= " AND photograph IS NOT NULL AND (photographValid = 0 || photographValid IS NULL)";
            }
        }
        return $where;
    }

    public function list($user = null, $name = null, $email = null, $post = null, $state = null, $city = null, $flag = null, $valid = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $email, $post, $state, $city, $flag, $valid, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, IFNULL(c.name, '-') AS name, IFNULL(c.email, '-') AS email, IFNULL(s.estado, '-') AS state, IFNULL(ci.cidade, '-') AS city, 
                IFNULL(DATE_FORMAT(c.requirementDate, '%d/%m/%Y'), '-') AS requirementDate, c.photographValid, c.videoValid, c.status,
                (CASE c.post
                    WHEN 1 THEN 'Deputado(a) Estadual/Distrital'
                    WHEN 2 THEN 'Deputado(a) Federal'
                    WHEN 3 THEN 'Governador(a)'
                    WHEN 4 THEN 'Prefeito'
                    WHEN 5 THEN 'Presidente'
                    WHEN 6 THEN 'Senador(a)'
                    WHEN 7 THEN 'Vereador'
                    WHEN 8 THEN 'Suplente de Senador(a)'
                    WHEN 9 THEN 'Vice Governador(a)'
                    WHEN 10 THEN 'Vice Presidente'
                    ELSE '-'
                END) AS post,
                (CASE 
                    WHEN photograph IS NOT NULL AND photographValid = 1 THEN 1
                    WHEN photograph IS NOT NULL AND (photographValid = 0 || photographValid IS NULL) THEN 2
                    ELSE '-'
                END) AS validation
                FROM eleicoes2022.candidates c
                LEFT JOIN states s ON s.id = c.state
                LEFT JOIN cities ci ON ci.id = c.city
                WHERE 1 = 1 {$where} 
                GROUP BY c.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($user = null, $name = null, $email = null, $post = null, $state = null, $city = null, $flag = null, $valid = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $email, $post, $state, $city, $flag, $valid, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(*)) AS total
                FROM eleicoes2022.candidates c
                LEFT JOIN states s ON s.id = c.state
                LEFT JOIN cities ci ON ci.id = c.city
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function findDetails($data)
    {
        $params = [];
        $where = '';
        if ($data['cargo']) {
            $params[':post'] = $data['cargo'];
            if($data['cargo'] != 6){
                $where .= " AND c.post IN (:post)";
            } else {
                $where .= " AND c.post IN (:post,8)";
            }
        } 
        if ($data['uf']) {
            $params[':state'] = $data['uf'];
            $where .= " AND c.state = :state";
        }
        if ($data['city']) {
            $params[':city'] = $data['city'];
            $where .= " AND c.city = :city";
        }
        if ($data['meso']) {
            $meso = $data['meso'];
            $params[':meso'] = $meso;
            $where .= " AND city IN (SELECT cityId FROM eleicoes2022.mesoregion WHERE meso = :meso)";
        }
        if ($data['bandeira']) {
            $params[':flag'] = $data['bandeira'];
            $where .= " AND fl.id = :flag";
        }
        if ($data['nome']) {
            $name = $data['nome'];
            $params[':name'] = "%$name%";
            $where .= " AND (c.nameMkt LIKE :name || c.name LIKE :name)";
        }
        if ( $data['gender']) {
            $params[':gender'] = $data['gender'];
            $where .= " AND c.gender = :gender";
        }
        $where2 = "c.post = 5";
        if($data['uf']){
            $uf = $data['uf'];
            $where2 = "c.post = 5 || c.post IN (3,6) AND c.state = $uf";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, IFNULL(c.nameMkt, '-') AS name, IFNULL(s.sigla, '-') AS state, IFNULL(ci.cidade, '') AS city, IFNULL(c.legend, '') legend, c.slug, c.status,
                (CASE c.post
                    WHEN 1 THEN 'deputadosEstaduais'
                    WHEN 2 THEN 'deputadosFederais'
                    WHEN 3 THEN 'governadores'
                    WHEN 4 THEN 'prefeitos'
                    WHEN 5 THEN 'presidente'
                    WHEN 6 THEN 'senadores'
                    WHEN 7 THEN 'vereadores'
                    WHEN 8 THEN 'suplenteDeSenador'
                    WHEN 9 THEN 'viceGovernador'
                    WHEN 10 THEN 'vicePresidente'
                    ELSE '-'
                END) AS post,
                (CASE c.photographValid
                    WHEN 1 THEN c.photograph
                    ELSE IF(c.gender = 1, 'gender_1.jpeg', 'gender_2.jpeg')
                END) AS photograph, IFNULL(b.banner, '') AS banner
                FROM eleicoes2022.candidates c
                LEFT JOIN states s ON s.id = c.state
                LEFT JOIN cities ci ON ci.id = c.city
                LEFT JOIN flags f ON f.candidate = c.id
                LEFT JOIN flagsList fl ON fl.id = f.flag
                LEFT JOIN banner b ON b.candidate = c.id
                WHERE {$where2} || 1 = 1 {$where}
                AND c.visible = 1 AND c.status = 1 GROUP BY c.id ORDER BY RAND()";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function findDetailsByDesignation($data)
    {
        $params = [];
        $where = '';
        if ($data['cargo']) {
            $params[':designation'] = $data['cargo'];
            $where .= " AND c.designation = :designation";
        }
        if ($data['uf']) {
            $params[':state'] = $data['uf'];
            $where .= " AND c.state = :state";
        }
        if ($data['city']) {
            $params[':city'] = $data['city'];
            $where .= " AND c.city = :city";
        }
        if ($data['meso']) {
            $meso = $data['meso'];
            $params[':meso'] = $meso;
            $where .= " AND city IN (SELECT cityId FROM eleicoes2022.mesoregion WHERE meso = :meso)";
        }
        if ($data['bandeira']) {
            $params[':flag'] = $data['bandeira'];
            $where .= " AND fl.id = :flag";
        }
        if ($data['nome']) {
            $name = $data['nome'];
            $params[':name'] = "%$name%";
            $where .= " AND (c.nameMkt LIKE :name || c.name LIKE :name)";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, IFNULL(c.nameMkt, '-') AS name, IFNULL(s.sigla, '-') AS state, IFNULL(ci.cidade, '') AS city, IFNULL(c.legend, '') legend, c.slug, c.status,
                (CASE c.designation
                    WHEN 1 THEN 'Estadual'
                    WHEN 2 THEN 'Federal'
                    WHEN 3 THEN 'Senado'
                    WHEN 4 THEN 'Governo'
                    WHEN 5 THEN 'Presidencia'
                END) AS designation,
                (CASE c.post
                    WHEN 1 THEN 'Deputado(a) Estadual/Distrital'
                    WHEN 2 THEN 'Deputado(a) Federal'
                    WHEN 3 THEN 'Governador(a)'
                    WHEN 5 THEN 'Presidente'
                    WHEN 6 THEN 'Senador(a)'
                    WHEN 8 THEN 'Suplente de Senador(a)'
                    WHEN 9 THEN 'Vice-Governador(a)'
                    WHEN 10 THEN 'Vice-Presidente'
                END) AS post,
                (CASE c.photographValid
                    WHEN 1 THEN c.photograph
                    ELSE IF(c.gender = 1, 'gender_1.jpeg', 'gender_2.jpeg')
                END) AS photograph, IFNULL(b.banner, '') AS banner
                FROM eleicoes2022.candidates c
                LEFT JOIN states s ON s.id = c.state
                LEFT JOIN cities ci ON ci.id = c.city
                LEFT JOIN flags f ON f.candidate = c.id
                LEFT JOIN flagsList fl ON fl.id = f.flag
                LEFT JOIN banner b ON b.candidate = c.id
                WHERE c.visible = 1 AND c.status = 1 {$where} 
                GROUP BY c.id ORDER BY RAND()";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function findRequirement($data)
    {
        $params = [];
        $where = '';
        if ($data['cargo']) {
            $params[':post'] = $data['cargo'];
            $where .= " AND c.post = :post";
        }
        if ($data['uf']) {
            $params[':state'] = $data['uf'];
            $where .= " AND c.state = :state";
        }
        if ($data['nome']) {
            $name = $data['nome'];
            $params[':name'] = "%$name%";
            $where .= " AND c.nameMkt LIKE :name";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, IFNULL(c.name, '-') AS name, IFNULL(s.sigla, '-') AS state, IFNULL(ci.cidade, '') AS city, IFNULL(c.legend, '') legend, c.slug, c.status,
                (CASE c.post
                    WHEN 1 THEN 'deputadosEstaduais'
                    WHEN 2 THEN 'deputadosFederais'
                    WHEN 3 THEN 'governadores'
                    WHEN 4 THEN 'prefeitos'
                    WHEN 5 THEN 'presidente'
                    WHEN 6 THEN 'senadores'
                    WHEN 7 THEN 'vereadores'
                    WHEN 8 THEN 'SuplenteDeSenador'
                    WHEN 9 THEN 'viceGovernador'
                    WHEN 10 THEN 'vicePresidente'
                    ELSE '-'
                END) AS post,
                (CASE c.photographValid
                    WHEN 1 THEN c.photograph
                    ELSE IF(c.gender = 1, 'gender_1.jpeg', 'gender_2.jpeg')
                END) AS photograph
                FROM eleicoes2022.candidates c
                LEFT JOIN states s ON s.id = c.state
                LEFT JOIN cities ci ON ci.id = c.city
                LEFT JOIN flags f ON f.candidate = c.id
                LEFT JOIN flagsList fl ON fl.id = f.flag
                WHERE c.requirementDate IS NOT NULL {$where}
                GROUP BY c.id ORDER BY RAND()";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function getCities($data)
    {
        $params = [];
        $where = '';
        if ($data['state']) {
            $params[':state'] = $data['state'];
            $where .= " AND ca.state = :state";
        }
        if ( $data['gender']) {
            $params[':gender'] = $data['gender'];
            $where .= " AND ca.gender = :gender";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ci.id, ci.cidade AS city FROM eleicoes2022.candidates ca
                LEFT JOIN cities ci ON ci.id = ca.city
                WHERE ca.visible = 1 AND ca.status = 1 AND ca.city IS NOT NULL {$where}
                GROUP BY ci.id";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function getPosts($data)
    {
        $params = [];
        $where = '';
        if ($data['state']) {
            $params[':state'] = $data['state'];
            $where .= " AND c.state = :state";
        }
        if ( $data['gender']) {
            $params[':gender'] = $data['gender'];
            $where .= " AND c.gender = :gender";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT post AS id, 
                    (CASE c.post
                        WHEN 1 THEN 'Deputado(a) Estadual/Distrital'
                        WHEN 2 THEN 'Deputado(a) Federal'
                        WHEN 3 THEN 'Governador(a)'
                        WHEN 4 THEN 'Prefeito'
                        WHEN 5 THEN 'Presidente'
                        WHEN 6 THEN 'Senador(a)'
                        WHEN 7 THEN 'Vereador'
                        WHEN 8 THEN 'Suplente de Senador(a)'
                        WHEN 9 THEN 'Vice Governador(a)'
                        WHEN 10 THEN 'Vice Presidente'
                    END) AS post FROM eleicoes2022.candidates c
                WHERE c.post != '' AND c.visible = 1 AND status = 1 {$where}
                GROUP BY c.post";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function getFlagsFilter($data)
    {
        $params = [];
        $where = '';
        if ($data['state']) {
            $params[':state'] = $data['state'];
            $where .= " AND c.state = :state";
        }
        if ( $data['gender']) {
            $params[':gender'] = $data['gender'];
            $where .= " AND c.gender = :gender";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT fl.id, fl.flag FROM eleicoes2022.candidates c
                LEFT JOIN flags f ON f.candidate = c.id
                LEFT JOIN flagsList fl ON fl.id = f.flag
                WHERE c.visible = 1 AND status = 1 AND fl.flag IS NOT NULL {$where}
                GROUP BY fl.flag";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function getSuplents($data)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, c.name, c.email, c.phone, c.post, s.estado AS state, ci.cidade AS city,
                    c.nameMkt, c.legend, c.crowdfunding, c.site, DATE_FORMAT(DATE_ADD(c.requirementDate, interval 10 day ) , '%d/%m/%Y') AS requirementDate, c.resume,
                    c.proposals, c.instagram, c.facebook, c.linkedin, c.twitter, c.tiktok, c.youtube,
                    c.bank, c.agency, c.account, c.cnpj, c.photograph, c.photographValid, c.video,
                    c.videoValid, c.qrCode, c.slug
                FROM eleicoes2022.candidates c
                LEFT JOIN states s ON s.id = c.state
                LEFT JOIN cities ci ON ci.id = c.city
                WHERE c.post = 8 AND c.visible = 1 and c.obs = :senator";
        $rows = $pdo->prepare($sql)->execute([':senator' => $data['senator']]);
        return $rows->fetchAllAssociative();
    }
}