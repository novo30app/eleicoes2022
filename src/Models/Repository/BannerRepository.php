<?php

namespace App\Models\Repository;
use App\Models\Entities\Banner;
use Doctrine\ORM\EntityRepository;

class BannerRepository extends EntityRepository
{
    public function save(Banner $entity):Banner
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}