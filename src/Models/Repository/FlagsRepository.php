<?php

namespace App\Models\Repository;
use App\Models\Entities\Flags;
use Doctrine\ORM\EntityRepository;

class FlagsRepository extends EntityRepository
{
    public function save(Flags $entity):Flags
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function refreshFlags($candidate)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM flags WHERE candidate = :candidate";
        $rows = $pdo->prepare($sql)->execute([':candidate' => $candidate->getId()]);
    }
}