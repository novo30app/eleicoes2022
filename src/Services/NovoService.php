<?php

namespace App\Services;

class NovoService
{
    public static function login(string $email, string $password): array
    {
        $data = array('email' => $email, 'password' => $password);
        $ch = curl_init('https://espaco-novo.novo.org.br/api/seletivo-2022/integracao');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        if ($result) {
            return json_decode($result, true);
        }
        return [];
    }
}