<?php

namespace App\Services;

class Auth
{
    private static $key = 'R&NMwtf5Xed2KnHNwQW1MAdLbZhrWeA77KvUpuL47f6JCVN1G2';
    private static $system = 'eleicoes';

    public static function login(string $email, string $password)
    {
        $authorization = base64_encode(self::$key) . ':' . base64_encode(self::$system);
        $headers = ['Content-Type: application/json', 'Accept: application/json', "X-Auth: {$authorization}"];
        $data = [
            'password' => $password,
            'email' => $email,
        ];
        $ch = curl_init('https://auth.novo.org.br/login');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (!$result['status'] || $result['status'] != 'ok') throw new \Exception($result['message']);
    }

    public static function sincronize(string $name, string $email, string $oldEmail)
    {
        $authorization = base64_encode(self::$key) . ':' . base64_encode(self::$system);
        $headers = ['Content-Type: application/json', 'Accept: application/json', "X-Auth: {$authorization}"];
        $data = [
            'name' => $name,
            'email' => $email,
            'oldEmail' => $oldEmail,
        ];
        $ch = curl_init('https://auth.novo.org.br/cadastro');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (!$result['status'] || $result['status'] != 'ok') throw new \Exception('Falha ao sincronizar auth');
    }

    public static function charge(string $name, string $email, string $password)
    {
        $authorization = base64_encode(self::$key) . ':' . base64_encode(self::$system);
        $headers = ['Content-Type: application/json', 'Accept: application/json', "X-Auth: {$authorization}"];
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];
        $ch = curl_init('https://auth.novo.org.br/carga');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (!$result['status'] || $result['status'] != 'ok') throw new \Exception('Falha ao sincronizar auth');
    }

}