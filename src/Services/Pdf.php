<?php

namespace App\Services;
use App\Helpers\Utils;
use Mpdf\Mpdf;

class Pdf
{
    public static function saint($state, $federal, $governor, $senator)
    {   
        $federalName = $federalLegend = '';
        if($federal){
            $federalName = strtoupper($federal->getNameMkt());
            $federalLegend = $federal->getLegend();
        }
        $stateName = $stateLegend = '';
        if($state){
            $stateName = strtoupper($state->getNameMkt());
            $stateLegend = $state->getLegend();
        }
        $governorNumber = $senatorNumber = '';
        if($governor) $governorNumber = 30;
        if($senator) $senatorNumber = 300;
        $template = BASEURL . '/assets/santinho.html';
        $mpdf = new mPDF();
        $mpdf->SetDisplayMode('fullpage');
        $template = file_get_contents($template);
        $template = str_replace('FEDERAL_NAME', $federalName, $template);
        $template = str_replace('FEDERAL_LEGEND', $federalLegend, $template);
        $template = str_replace('STATE_NAME', $stateName, $template);
        $template = str_replace('STATE_LEGEND', $stateLegend, $template);
        $template = str_replace('GOVERNOR', $governor, $template);
        $template = str_replace('GNUMBER', $governorNumber, $template);
        $template = str_replace('SENATOR', $senator, $template);
        $template = str_replace('SNUMBER', $senatorNumber, $template);
        $mpdf->WriteHTML($template);
        $mpdf->Output('Colinha-NOVO.pdf','D');
        exit;
    }
}