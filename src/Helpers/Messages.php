<?php

namespace App\Helpers;

class Messages
{
    public static function userAdminMsg($userAdmin, $recoverPassword)
    {
        return "Olá {$userAdmin->getName()},<br>
                O Comitê acaba de cadastrar você em nosso portal de candidatos do site do NOVO.<br>
                Para ter acesso a plataforma você deve usar as mesmas credenciais de seu Espaço NOVO,
                <a href='https://eleicoes2022.novo.org.br/ativar/{$recoverPassword->getToken()}'' target='_blank'>Clique aqui</a> para acessar.<br>
                Por esse sistema você poderá incluir, editar e divulgar suas informações de candidatura que serão disponibilizadas no site do NOVO.<br>
                Recomendamos muita atenção no preenchimento e nas orientações cadastradas no “Home”.<br>
                Qualquer dúvida estamos à disposição via comite@novo.org.br.<br>
                Atenciosamente,";
    }

    public static function newRequirement($candidate)
    {
        return "Olá,<br>
                Uma nova solicitação de requerimento de impugnação foi realizada para o pré-candidato {$candidate->getName()}.<br>
                <a href='https://eleicoes2022.novo.org.br/requerimentos'>Clique aqui</a> Para maiores detalhes";
    }
}